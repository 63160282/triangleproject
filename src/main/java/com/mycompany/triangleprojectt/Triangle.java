/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.triangleprojectt;

/**
 *
 * @author User
 */
public class Triangle {
    double B;
    double H;
    static final double pi = 0.5;
    public Triangle(double B,double H){
        this.B=B;
        this.H=H;          
    }
    public double TriangleArea(){
        return pi*B*H;
    }
    public void setR(double B,double H){
        if(H==0 || B==0){
            System.out.println("Error");
            return;
        }
        this.B=B;
        this.H=H;
    }
    public double getR(){
        return pi*B*H;
    }
    
}
